# django-docker-setup
This is a template Django at docker containers using docker compose with NGINX, Gunicorn and Postgres.

Uses Docker Compose to define some dockers at the architecture. NGINX web server at port 80, media and static
server. Postgresql as database server.

To test the docker structure run `docker-compose up`.
